import asyncio
from duckpy import AsyncClient
from duckpy import Client

def test_functional():
     client = Client()
     results = client.search("Python Wikipedia")
     # Assert first result title is not empty
     assert results[0]['title'] != ""

     # Assert first result URL is not empty
     assert results[0]['url'] != ""

     # Prints first result description
     assert results[0]['description'] != ""


async def get_asyncio_results():
    client = AsyncClient()
    results = await client.search("Debian")

    # Assert first result title is not empty
    assert results[0]['title'] != ""

    # Assert first result URL is not empty
    assert results[0]['url'] != ""

    # Assert first result description is not empty
    assert results[0]['description'] != ""


def test_asyncio_results():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(get_asyncio_results())
